import socket
import sys
import os
sys.path.append(os.path.dirname(os.path.abspath(__file__)))
sys.path.append('../')
from ImqClient.Client.AppConstants import *
from ImqClient.Client.ClientMessageController import *
from ImqClient.Client.ClientService import *
from ImqClient.Client.Exception.ClientException import *

class ClientHandler:
    def startClient(self):
        try:   
            client=ClientService()
            client.connectClientToServer()
            client.getWelcomeMessageFromServer()
            client.communicateWithServer()
        except ClientException as exception:
            print(exception.args[0])

clientHandler=ClientHandler()
clientHandler.startClient()
